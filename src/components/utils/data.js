const initialData = {
    "formSchema": [{
            "label": "First Name",
            "type": "text"
        },
        {
            "label": "Last Name",
            "type": "text"
        },
        {
            "label": "Email",
            "type": "email"
        },
        {
            "label": "Age",
            "type": "number"
        },
        {
            "label": "Date of Birth",
            "type": "date"
        },
        {
            "label": "Gender",
            "options": ["male", "female", "other"],
            "type": "radio"
        }
    ]
}

export default initialData;