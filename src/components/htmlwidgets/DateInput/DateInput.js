import * as React from "react";
import Text from "../TextInput";

type Props = {
    id: string,
    label: string,
    required? : boolean,
    [string]: string,

}
/**
 * 
 * @param {*} props 
 * DateInput is a date component
 */
function DateInput(props: Props) {
    const {id,label,required,...restProps} = props
    
    //Check if the given date is not less than 18years from today
    var currentDate = new Date();
    var eighteenYearsAgo = new Date(currentDate.setFullYear(currentDate.getFullYear()-18))
                        .toISOString().split("T")[0];
    return (
        <div>
            <Text id={id} type="date" label={label} required={required} {...restProps} max={eighteenYearsAgo}/>
        </div>
    )
}

export default DateInput;