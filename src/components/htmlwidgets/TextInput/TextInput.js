import React,{type ComponentType} from "react";
import Label from "../Label";
import styled from "styled-components";


const Margin : ComponentType<any> = styled.div`
    margin: 10px;
    display: ${props => props.hide ? "none" : "" }
`;

const Input = styled.input`
    margin-top: 3px;
    width : 80%;
    height: 2em;
    font-size: 18px;
    border: none;
    border: 1px solid #23629f;
    outline: none;
    border-radius: 10px;
    padding-left: 1em;
`;

type Props = {
    id : string,
    label : string,
    hide?: boolean,
    type? : string,
    required? : boolean,
    [string] : string
}

/**
* Core component of text
*/
function TextInput(props: Props) {
    const {id,label,type,hide,required,...restProps} = props
    return (
        <Margin hide={hide}>
            <Label htmlFor={id} label={label} required={required}/>
            <Input 
                id={id} 
                type={type} 
                placeholder={label}
                data-testid={id} 
                required={required} {...restProps} />
        </Margin>
    )
}

TextInput.defaultProps = {
    type : "text",
    required : true,
    hide: false
};

export default TextInput;