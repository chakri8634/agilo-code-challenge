import React from "react";
import Text from "../TextInput";


type Props = {
    id : string,
    label: string,
    required : boolean,
    [string] : string
}

/**
 * 
 * @param {*} props 
 * EmailInput : email component which composes input component
 */
function EmailInput(props: Props) {
    const {id,label,required=true,...restProps}  = props 
    return (
        <div>
            <Text id={id} type="email" label={label} required={required} {...restProps} />
        </div>
    )
}

EmailInput.defaultProps = {
    required: true
}
export default EmailInput;