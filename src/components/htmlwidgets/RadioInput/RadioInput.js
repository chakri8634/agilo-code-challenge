import React,{type ComponentType} from "react";
import Label from "../Label";
import styled from "styled-components";

const Margin : ComponentType<any> = styled.div`
    margin: 10px;
`;

type Props  = {
    id : string,
    label : string,
    required : boolean,
    hide: boolean,
    options? : ?string[],
    [string] : string
}

/**
 * 
 * @param {*} props 
 * RadioInput component is clone of input type radio
 */
function RadioInput(props: Props) {
    const {id,label,required,options,hide,...restProps} = props
    return (
        <Margin data-testid={"radio_"+id} hide={hide}>
            <Label htmlFor={id} label={label} required={required}/>
            <React.Fragment key={label}>
                {(options != null && options != undefined && options.length > 0) && options.map((m,i) => {
                    return (
                        <React.Fragment key={"radio"+i}>
                            <input 
                                data-testid={"radio"+i} 
                                id={"radio"+i} type={"radio"} value={m} name={label} required={required}/> {m}
                        </React.Fragment>
                    )
                    })
                }
            </React.Fragment>
        </Margin>
    )
}

RadioInput.defaultProps = {
    required : true,
    options : []
}

export default RadioInput;