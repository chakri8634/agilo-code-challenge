import React,{type ComponentType} from "react";
import styled from "styled-components";


const BoldLabel:ComponentType<any> = styled.label`
    display: block;
    font-weight: bold;
`;

const RedStar:ComponentType<any> = styled.span`
    display: inline;
    color: red;
`;


const Mandatory = () => <RedStar>*</RedStar>

type Props = {
    label: string,
    htmlFor: string,
    required : boolean
}
/**
 * 
 * @param {*} props 
 * Label : label component which is used along with other inputs
 */
function Label(props: Props) {
    const {label,htmlFor,required} = props
    return <BoldLabel 
                data-testid={"lbl_"+htmlFor}
                htmlFor={htmlFor}>{label} {required && <Mandatory /> }</BoldLabel>
}

Label.defaultProps = {
    required: true
}

export default Label;