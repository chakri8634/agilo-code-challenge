import React from "react";
import Text from "../TextInput";

type Props = {
    id : string,
    label: string,
    required : boolean,
    [string] : string
}

/**
 * 
 * @param {*} props 
 * NumberInput : number component which composes input component
 */
function NumberInput(props: Props){
    const {id,label,required,...restProps} = props
    return (
        <div>
            <Text id={id} type="number" label={label} required={required} {...restProps} />
        </div>
    )
}

NumberInput.defaultProps = {
    required : true
}

export default NumberInput;