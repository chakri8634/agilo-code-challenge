import React,{type ComponentType} from "react";
import styled from "styled-components";


const Img: ComponentType<any> = styled.img`
    width: 96px;
    margin-left: 20px;
`;

type ColorProps = {
    color: string
}

const StyledHeader : ComponentType<ColorProps> = styled.header`
    flex: 1;
    padding: 5px;
    background-color: ${props => props.color};
`;


type HeaderProps = {
    logo : string,
    bgColor: string
}

/**
 * 
 * @param {*} props 
 * Header componet for displaying the header of the page
 */
function Header(props: HeaderProps) {
    const {logo,bgColor} = props
    return (
      <StyledHeader data-testid="header" color={bgColor}> 
            <Img
                src={logo}
                alt={"app-logo"}
            />
        </StyledHeader>
    )
}

Header.defaultProps = {
    bgColor: "#23629f"
};

export default Header;