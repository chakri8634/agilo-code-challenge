import React,{useState,type ComponentType} from "react";
import CreateForm from "./CreateForm";

import styled from "styled-components";


// styles
const Error:ComponentType<any> = styled.div`
    color: red;
`;

// styles for form render
const Button = styled.input`
    background: #23629f;
    border-radius: 4px;
    width: 100px;
    height: 40px;
    border: 2px solid white;
    color: white;
    margin: 0 1em;
    padding: 0.25em 1em;
    font-size: 1em;
`;

type Props = {
    jsonSchema: any,
    uiValidation: any,
    setFormError: any
}

function FormRender(props: Props) {
    // state of json schema
    const {jsonSchema,uiValidation,setFormError} = props
    // state of form errors when form submitted
    const [formError,setFormErrors] = useState(null)
    // state of all hidden inputs when the input is valid
    const [hiddenMap,setHiddenMap] = useState({})

    let validJson = {};
    let validUIValidation = null;
    // check if json is valid
    const isValidJson = () => {
        try {
            validJson = JSON.parse(jsonSchema);
        }catch(e) {
            return false;
        }
        return true;
    }

    // check if json is valid
    const isValidUIValidation = () => {
        try {
            validUIValidation = JSON.parse(uiValidation);
        }catch(e) {
            return false;
        }
        return true;
    }

    // Creates form and composes CreateForm
    const renderJson = () => {
        return (
            isValidJson() ? 
                            <CreateForm schema={validJson['formSchema']} 
                                validations={isValidUIValidation() ? validUIValidation : {}}
                                hiddenMap={hiddenMap} />
                          : <h1>Invalid Json</h1>
        );
    }
    
    // To hide the elements which are valid
    const mockAPI = (e) => {
        const form = e.target;
        let isValid = true;
        let newHiddenMap = {}
        for(let el of form.elements){
            if(!el.validity.valid){
                isValid = false;
                newHiddenMap[el.id] = false
            }else{
                if(el.id !== undefined && el.id !== null && el.id.length > 0){
                    newHiddenMap[el.id] = true
                }
            }
        }
        setHiddenMap(newHiddenMap)
        setFormError(isValid)
        return isValid
    }

    const onFormSubmit = (e) => { 
        e.preventDefault();
        setFormErrors(!mockAPI(e))
    }

    return (
        <form onSubmit={e => onFormSubmit(e)} noValidate>
            <Error>{formError ? "Some fields were not correctly filled" : null}</Error>
            {renderJson()}
            <Button 
                data-testid="submit-form"
                type="submit" />
        </form>
    )
}

export default FormRender;