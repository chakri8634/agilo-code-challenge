import React,{type ComponentType} from "react";
import styled from "styled-components";

const JsonTextArea:ComponentType<any> = styled.textarea`
    width: 100%;
    min-height: ${(props) => props.height};
    font-size: 18px;
    border: 1px solid #23629f;
`;


type Props = {
    stateUpdate : any,
    initData : any,
    height? : string,
    label : string
}

/**
 * 
 * @param {*} props 
 * Input form where use enters the json schema
 */
function InputForm(props: Props){
    const {stateUpdate,initData,height,label} = props;
    return (
        <div>
            <label>
                {label}
                <JsonTextArea
                    height = {height}
                    defaultValue={initData}
                    onChange={(e) => stateUpdate(e.target.value)} />
            </label>
        </div>
    )
}


InputForm.defaultProps = {
    height : "500px"
};

export default InputForm;