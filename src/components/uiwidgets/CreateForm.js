import React from "react";
import TextInput from "../htmlwidgets/TextInput";
import NumberInput from "../htmlwidgets/NumberInput";
import EmailInput from "../htmlwidgets/EmailInput";
import DateInput from "../htmlwidgets/DateInput";
import RadioInput from "../htmlwidgets/RadioInput";


// genete hashcodes for id
const hashCode = (s) => {
    for(var i = 0, h = 0; i < s.length; i++)
        h = Math.imul(31, h) + s.charCodeAt(i) | 0;
    return h;
} 

type InputProps = {
    id: string,
    type : string,
    hide: any,
    [string] : string
}
/** 
 Factory kind of pattern which returns the requested component
 based on the type
 e.g., 
    email => EmailInput
    date => DateInput
*/
function GenericInput(props: InputProps){
    const {id,type,label,hide,...restProps} = props

    let lid = id;
    if(id===undefined){
        lid = "id"+hashCode(label)
    }
    switch(type){
        case 'input':
            return <TextInput id={lid}  hide={hide[lid]} label={label} {...restProps} />
        case 'number':
            return <NumberInput id={lid}  hide={hide[lid]} label={label} {...restProps} />
        case 'email':
            return <EmailInput id={lid}  hide={hide[lid]} label={label} {...restProps} />
        case 'date':
            return <DateInput id={lid} hide={hide[lid]} label={label} {...restProps} />
        case 'radio':
            return <RadioInput id={lid} hide={hide[lid]} label={label} {...restProps} />
        default:
            return <TextInput id={lid}  hide={hide[lid]} label={label} {...restProps}/>
    }
}   

type Props = {
    schema : any,
    validations : any,
    hiddenMap : any
}
/**
 * Helper function for taking schema and validations and rendering Form
 */
function CreateForm(props: Props) {
    const {schema,validations,hiddenMap} = props
    return (
        <React.Fragment>
            {schema.map((el,i) => {
                    return (
                        <GenericInput key={i}
                            {...el}
                            {...(validations !== null && validations.hasOwnProperty(el["type"])) ? validations[el["type"]] : null}
                            hide={hiddenMap}/>
                    )
             })
            }
        </React.Fragment>
    )
}

export default CreateForm;
