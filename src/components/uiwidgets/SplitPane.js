import React,{useState} from "react";
import styled,{type StyledComponent} from "styled-components";
import InputForm from "./InputForm";
import FormRender from "./FormRender";
import initialData from "../utils/data";
import uiValidations from "../validation/patterns";

const prettyJson = (jsonObject) => JSON.stringify(jsonObject,undefined,4);
// styled components
const SplitUI: StyledComponent<{},{},HTMLDivElement>  = styled.div`
    display : flex;
    flex-direction: row;
    padding :  20px;
`;

const HalfScreenView : StyledComponent<{},{},HTMLDivElement> = styled.div`
    width: 50%;
    margin-right: 30px;
`;

/**
 * 
 * @param {*} props 
 * Split ui which splits both input schema and json form render side by side
 */
function SplitPane() {
    const [jsonSchema,setJsonSchema] = useState(prettyJson(initialData))
    const [uiValidation,setUiValidation] = useState(prettyJson(uiValidations))
    const [formError,setFormError] = useState(null)
    return (
        (formError != null && formError) ? 
        <h1>Form successfully submitted!</h1> :
        <SplitUI data-testid="main-pane">
            <HalfScreenView>
                <InputForm 
                    stateUpdate={setJsonSchema}
                    initData={jsonSchema}
                    label={"JSON Schema"}
                />
                <InputForm height="300px"
                    stateUpdate={setUiValidation}
                    label={"UI Validations"}
                    initData={uiValidation}
                />  
            </HalfScreenView>
            <HalfScreenView>
                <FormRender 
                    jsonSchema={jsonSchema}
                    uiValidation={uiValidation}
                    setFormError={setFormError}
                />
            </HalfScreenView>
    </SplitUI>
    )
}

export default SplitPane;