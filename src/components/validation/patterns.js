const uiValidations = {
    "text" : {
        "maxLength" : 20
    },
    "number" : {
        "min" : 18,
        "max" : 60
     },
     "email" : {
        "maxLength" : 21,
        "pattern" : "(.*)\.(.*)@google\.com",
        "title"  : "Should be of format *.*@google.com"
     }
}

export default uiValidations;