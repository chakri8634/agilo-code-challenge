//@flow
import React from 'react';
import logo from './logo.png';
import './App.css';
import {Header,SplitPane} from "./components/uiwidgets";

// Main App goes here
function App() { 
  return (
    <div>
      <Header logo={logo}/>
      <SplitPane />
    </div>
  );
}

export default App;
