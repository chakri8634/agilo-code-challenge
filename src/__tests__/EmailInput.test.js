import React from "react";
import {render,cleanup} from "@testing-library/react";
import EmailInput from "../components/htmlwidgets/EmailInput";
import '@testing-library/jest-dom/extend-expect'


afterEach(cleanup);

test("EmailInput rendering for required=true",() => {
    const id = "email"
    const {getByTestId,asFragment} = render(<EmailInput id={id} label={"Email"}/>)

    expect(getByTestId(id).type).toBe(id)
    expect(getByTestId(id).id).toBe(id)
    expect(getByTestId(id).required).toBe(true)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});


test("EmailInput rendering for required=false",()=>{
    const id = "email"
    const {getByTestId,asFragment} = render(<EmailInput id={id} label={"Email"} required={false} />)
    expect(getByTestId(id).type).toBe(id)
    expect(getByTestId(id).id).toBe(id)
    expect(getByTestId(id).required).toBe(false)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});