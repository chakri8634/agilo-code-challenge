import React from "react";
import {Header,SplitPane} from "../components/uiwidgets"
import {render,cleanup,fireEvent} from "@testing-library/react";
import logo from "../logo.png";
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup);

test("Header should have logo and color",() => {
    const {getByAltText,asFragment,getByTestId} = render(<Header logo={logo} bgColor={"#ffffff"} />)
    const imgElement = getByAltText(/app-logo/);
    const header  = getByTestId("header")
    expect(imgElement.src).toContain("logo.png")
    expect(header).toHaveStyle(`
        color: "#ffffff"
    `);
    expect(asFragment()).toMatchSnapshot();
});


test("Main pane test",() => {
    const {asFragment,getByTestId} = render(<SplitPane />)
    expect(getByTestId("main-pane")).toHaveTextContent("First Name")
    expect(getByTestId("main-pane")).toHaveTextContent("Last Name")
    expect(getByTestId("main-pane")).toHaveTextContent("Age")
    expect(getByTestId("main-pane")).toHaveTextContent("Date of Birth")
    expect(getByTestId("main-pane")).toHaveTextContent("Gender")
    expect(getByTestId("main-pane")).toHaveTextContent("Email")
    expect(asFragment()).toMatchSnapshot()
});

/**
 * Test case for validating wether correct inputs hide the form
 */
test("Input element should hide on successfull input",() => {
    const {asFragment,getByTestId,getByPlaceholderText,debug} = render(<SplitPane />)
    const firstNameInput = getByPlaceholderText("First Name")
    fireEvent.change(firstNameInput,{target : { value : "hdjashdjsahjdhjsahdjhasj"}})
    fireEvent.click(getByTestId("submit-form"))
    expect(firstNameInput.closest("div")).toHaveStyle(`
        display: none;
    `)
    expect(asFragment()).toMatchSnapshot()
});

/**
 * Test case for validating wether incorrect shows error text
 */
test("Error message if any invalid data in the form",() => {
    const {asFragment,getByTestId,getByPlaceholderText,debug} = render(<SplitPane />)
    const firstNameInput = getByPlaceholderText("First Name")
    fireEvent.change(firstNameInput,{target : { value : "123451234512345"}})
    fireEvent.click(getByTestId("submit-form"))
    expect(firstNameInput.closest("div")).toHaveStyle(`
        display: none;
    `)
    expect(getByTestId("main-pane")).toHaveTextContent("Some fields were not correctly filled")
    expect(asFragment()).toMatchSnapshot()
});

/**
 * Successful submission of form should give a success message
 */
test("Successfuly submitted form",() => {
    const {asFragment,getByText,getByTestId,getByPlaceholderText,debug} = render(<SplitPane />)
    fireEvent.change(getByPlaceholderText("First Name"),{target : { value : "123451234512345"}})
    fireEvent.change(getByPlaceholderText("Last Name"),{target : { value : "123451234512345"}})
    fireEvent.change(getByPlaceholderText("Email"),{target : { value : "chakri@google.com"}})
    fireEvent.change(getByPlaceholderText("Date of Birth"),{target : { value : "1987-06-12"}})
    fireEvent.change(getByPlaceholderText("Age"),{target : { value : 31}})
    fireEvent.change(getByTestId("radio0"),{target : { checked : true}})
    fireEvent.click(getByTestId("submit-form"))
    expect(getByText("Form successfully submitted!")).toBeTruthy()
    expect(asFragment()).toMatchSnapshot()
});