import styled from 'styled-components'
import React from "react";
import {render,cleanup} from "@testing-library/react";
import DateInput from "../components/htmlwidgets/DateInput";
import '@testing-library/jest-dom/extend-expect'


afterEach(cleanup);

test("DateInput rendering for required=true",() => {
    const {getByLabelText,getByTestId,asFragment} = render(<DateInput id={"dob"} label={"Date of birth"}/>)
    expect(getByTestId("dob").type).toBe("date")
    expect(getByTestId("dob").required).toBe(true);
    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});


test("DateInput rendering for required=false",()=>{
    const {getByLabelText,getByTestId,asFragment} = render(<DateInput id={"dob"} label={"Date of birth"} required={false} />)
    expect(getByTestId("dob").type).toBe("date");
    expect(getByTestId("dob").required).toBe(false);
    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});