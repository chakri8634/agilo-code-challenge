import styled from 'styled-components'
import React from "react";
import {render,cleanup} from "@testing-library/react";
import TextInput from "../components/htmlwidgets/TextInput";
import '@testing-library/jest-dom/extend-expect'


afterEach(cleanup);

test("TextInput rendering for required=true",() => {
    const {getByLabelText,getByTestId,asFragment} = render(<TextInput id={"first_name"} label={"First Name"}/>)
    expect(getByLabelText("First Name *")).toBeTruthy()
    expect(getByTestId("first_name").type).toBe("text")
    expect(getByTestId("first_name").id).toBe("first_name")
    expect(getByTestId("first_name").required).toBe(true)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});


test("TextInput rendering for required=false",()=>{
    const {getByLabelText,getByTestId,asFragment} = render(<TextInput id={"first_name"} label={"First Name"} required={false} />)
    expect(getByLabelText("First Name")).toBeTruthy()
    expect(getByTestId("first_name").type).toBe("text")
    expect(getByTestId("first_name").id).toBe("first_name")
    expect(getByTestId("first_name").required).toBe(false)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});