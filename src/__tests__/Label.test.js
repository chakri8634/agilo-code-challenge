import styled from 'styled-components'
import React from "react";
import {render,cleanup} from "@testing-library/react";
import Label from "../components/htmlwidgets/Label";
import '@testing-library/jest-dom/extend-expect'


afterEach(cleanup);

test("Label rendering for required=true",() => {
    const id = "label_id"
    const {getByTestId,asFragment} = render(<Label htmlFor={id} label={"First Name"}/>)
    expect(getByTestId("lbl_"+id)).toHaveTextContent("*")
    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});


test("Label rendering for required=false",() => {
    const id = "label_id"
    const {getByTestId,asFragment} = render(<Label htmlFor={id} label={"First Name"} required={false}/>)
    expect(getByTestId("lbl_"+id)).not.toHaveTextContent("*")
    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});