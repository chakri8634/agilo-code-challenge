import React from "react";
import {render,cleanup} from "@testing-library/react";
import RadioInput from "../components/htmlwidgets/RadioInput";
import '@testing-library/jest-dom/extend-expect'


afterEach(cleanup);

test("RadioInput rendering for required=true",() => {
    const {asFragment,getByDisplayValue} = render(<RadioInput id={"gender"} label={"Gender"} options={["male","female"]}/>)
    expect(getByDisplayValue("female")).toBeInTheDocument()
    expect(getByDisplayValue("male")).toBeInTheDocument()
    expect(asFragment()).toMatchSnapshot()
});