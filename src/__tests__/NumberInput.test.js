import styled from 'styled-components'
import React from "react";
import {render,cleanup} from "@testing-library/react";
import NumberInput from "../components/htmlwidgets/NumberInput";
import '@testing-library/jest-dom/extend-expect'



afterEach(cleanup);

test("NumberInput rendering for required=true",() => {
    const {getByTestId,asFragment} = render(<NumberInput id={"age"} label={"Age"}/>)
    

    expect(getByTestId("age").type).toBe("number")
    expect(getByTestId("age").id).toBe("age")
    expect(getByTestId("age").required).toBe(true)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});


test("NumberInput rendering for required=false",()=>{
    const {getByTestId,asFragment} = render(<NumberInput id={"age"} label={"Age"} required={false} />)
    
    expect(getByTestId("age").type).toBe("number")
    expect(getByTestId("age").id).toBe("age")
    expect(getByTestId("age").required).toBe(false)

    // save the snapshot
    expect(asFragment()).toMatchSnapshot()
});