This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Styled components
1. Styled components library used for styling of the components wherever it is needed.


## Flow
1. Flow is used 
2. To run flow run `yarn run flow`
   

## Testing
1. Test is done on both components and as functionality whole
2. To run tests run `yarn run test`
3. Currently test also capture snapshots.If you come across any issue with snapshot please update it by pressing **U**

